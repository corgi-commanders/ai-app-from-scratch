## CODE RED! Level Up Your App with GitLab Duo AI in Under 15 Minutes

![siren](codered.gif)


You, a coding guru known for blazing through JavaScript, just got ambushed by your project manager. Not with a latte and a pep talk, but with a mission-critical feature: a user management app using REST APIs. Due? **By the end of the day.** Language of choice? **Python (gulp)**.

Panic? Not a chance! This is your chance to become a full-stack legend. Python may be uncharted territory but you have GitLab Duo on your side. 

So, grab your keyboard, channel your inner Python guru, and get ready to write some heroic code. This quick app challenge is about to become an epic win thanks to GitLab + GitLab Duo.


[![Video](https://yt3.ggpht.com/R6P5skGdZJeM1bebvt3ILeU8k-9tiqE5T198RmBH8SoGXH2gk_Lk-45uZoq6X6pW4a4c9Sqn=s88-c-k-c0x00ffffff-no-rj)](https://youtu.be/o2xmLTV1y0I)

*Click Tanuki to access GitLab Duo Video.*


-------------
## Time is ticking! Buckle up and become an app-building legend with the App From Scratch To-Do List:

1.  **Feature Request!**   

> - Navigate to Issues (plan >  Issues)
> - Click New Issue (+🆕)
> - Title: "Create User Management App using Python with RestAPIs" ️
> - Preview with Tanuki Icon  (click icon)
> - Generate Description: "Create User Management App Using Python with RestAPIs - Due today" (in popup) ✍️⏰
> - Submit (check mark ✅) - Description populates!
> - Assign (person icon ‍) & Set Due Date (calendar )
> - Create Issue (check mark again ✅)



2.  **Python Power Up!**    Create a new Python file named `appmanagement.py` and packages file, `requirements.txt`

3.  **Code Like a Boss!**   Utilize code suggestions to generate some awesome code (think magic! ✨):

```python
# Create a Flask webserver   
# Add REST API endpoints to manage users by ID  
# Implement create, update, and delete functions for users  
# Store user data securely in an SQLite database 
# Run the server on port 8080 with TLS enabled for extra security 
# Print the required packages for easy installation in a requirements.txt file 
```

4.  **Explain It Like I'm Five!**  ‍   Break down this User Management App code into simpler terms for everyone to understand by right clicking > GitLab Duo Chat to `Explain Selected Code`. [Prompt: /explain]

6.  **Testing, Testing!**  1️⃣2️⃣3️⃣   Generate a test for the User Management App by right clicking > GitLab Duo Chat to `Generate Tests` and add it to a new file named `appmanagement_test.py`.  (Imagine a tiny robot mimicking how users interact with the app). [Prompt: /tests]

7.  **Refactor Like a Pro**   ️  Get advice on how to refactor the code to make it run super fast! Simplify by right clicking > GitLab Duo Chat to `Refactor` for some optimization magic and sets up your code for future expansion! [Prompt: /refact]

**Open appmanagement.py and let's use GitLab Duo Chat. [Prompt: /clear]**

8.  **Document It Like a Champ!**     Create comprehensive documentation for `appmanagement.py` by asking Duo Chat: 
`Create Documentation for appmanagement.py`

9.  **Wiki Power!**      Save your amazing documentation in a [GitLab Wiki](https://docs.gitlab.com/ee/user/project/wiki/).  (Think of it as an online encyclopedia for your code). 
Title is `Documentation for User App Management` with Summary: 

```This is a basic REST API that provides a standardized interface for managing user data stored in a local SQLite database.```




## ✅  CODE RED: Developer Superhero Accomplishments!


- [ ] Conquered a New Language... Fast! (Used a new coding language to create the app, under pressure.)
- [ ] Code Guru on Speed Dial! ⚡️ (Understood the codebase, even in a new language, in record time.)
- [ ] Testing Titan: Blitz Edition!  (Ensured the app was bug-free with super-fast testing.)
- [ ] Future-Proofed: Think Big, Act Fast!  (Got advice on how to scale the app for future growth, on the fly.)
- [ ] Documented Like a Boss (Against the Clock)! ⏱️ (Created clear documentation for the app, lightning fast.)
- [ ] Collaboration Champion: Shared the Glory!  (Created a wiki to share knowledge and boost teamwork, instantly.)

<<< What are some ways you can see this workflow in your organization? >>>

-------------
## Are you thirsty for more? 

<img src="more-thirsty.gif">

Credit to [Tenor Gif](https://tenor.com/bw5Lj.gif).


💪 Don't worry, you're not alone. 

### Let's deep dive into using GitLab Duo AI features for security with the <a href="https://gitlab.com/kkwentus1-demo/ai-app-from-scratch/-/tree/main/notes_app" target="_blank">Flask app security</a>


